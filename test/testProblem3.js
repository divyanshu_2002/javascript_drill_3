const finduserWithMaster = require("../Problem3");

const users = require("../data");

try {
    const result = finduserWithMaster(users);
    if (result) {
        console.log(`The users with masters are ${result}`);
    }
    else {
        console.log(`pass the proper object to get the users in masters`);
    }

}
catch (error) {
    console.log(error.message);
}