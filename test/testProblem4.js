const groupByLanguage = require("../Problem4");
const data = require("../data");

try {
    const result = groupByLanguage(data);
    if (result) {
        console.log(`Grouped users are`);
        console.log(result);
    }
    else {
        console.log(`pass the valid object to group`);
    }
}
catch (error) {
    console.log(error.message);
}
