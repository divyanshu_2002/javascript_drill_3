function interestedInVideoGames(users) {
  if (typeof users == "object" && typeof users != null) {
    const allUser = [];
    for (user in users) {
      if (users[user].interests[0].includes("Playing Video Games") || users[user].interests[0].includes("Video Games")) {
        allUser.push(user);
      }
    }
    return allUser;
  }
  else {
    return null;
  }
}


module.exports = interestedInVideoGames;