function finduserWithMaster(users) {
    if (typeof users == "object" && typeof users != null) {
        const userWithMaster = [];
        for (user in users) {
            if (users[user].qualification == "Masters") {
                userWithMaster.push(user);
            }
        }
        return userWithMaster;
    }
    else {
        return null;
    }
}

module.exports = finduserWithMaster;