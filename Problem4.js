
let group = {
    Golang: [],
    Javascript: [],
    Python: [],

};

function groupByLanguage(users) {
    if (typeof users == "object" && typeof users != null) {
        for (user in users) {
            const techstack = users[user].desgination;
            if (techstack.includes("Python")) {
                group.Python.push(user);
            }
            else if (techstack.includes("Javascript")) {
                group.Javascript.push(user);
            }
            else if (techstack.includes("Golang")) {
                group.Golang.push(user);
            }
        }
        return group;
    }
    else {
        return null;
    }
}

module.exports = groupByLanguage;
