// Q2 Find all users staying in Germany.


function finduserInGermany(users) {
    if (typeof users == "object" && typeof users != null) {
        const userIngermany = [];
        for (user in users) {
            if (users[user].nationality == "Germany") {
                userIngermany.push(user);
            }
        }
        return userIngermany;
    }
    else{
        return null;
    }
}

module.exports = finduserInGermany;